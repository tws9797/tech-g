-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2021 at 02:19 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_22_075201_create_teachers_table', 1),
(5, '2021_06_22_075217_create_students_table', 1),
(6, '2021_06_22_080526_create_test_results_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` bigint(20) UNSIGNED NOT NULL,
  `class_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `teacher_id`, `class_level`, `race`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Name Hirthe I', 1, 'freshman', 'indians', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(2, 'Orin Watsica MD', 1, 'freshman', 'indians', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(3, 'Dr. Valentin Oberbrunner', 1, 'sophomores', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(4, 'Prof. Rigoberto Dare V', 1, 'juniors', 'indians', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(5, 'Anissa Ward DVM', 1, 'juniors', 'malays', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(6, 'Abby Keeling Sr.', 2, 'freshman', 'malays', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(7, 'Emelia Sanford PhD', 2, 'freshman', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(8, 'Prof. Laurence Olson', 2, 'seniors', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(9, 'Antone Mante', 2, 'juniors', 'indians', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(10, 'Macie Ledner', 2, 'sophomores', 'malays', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(11, 'Frederick Wolf', 3, 'juniors', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(12, 'Therese Purdy', 3, 'juniors', 'indians', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(13, 'Arlie Smith DDS', 3, 'seniors', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(14, 'Sofia Gerlach II', 3, 'freshman', 'malays', '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(15, 'Adriana Balistreri', 3, 'juniors', 'chinese', '2021-06-22 03:35:45', '2021-06-22 03:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `position`, `age`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Pedro Reinger', 'second', 37, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(2, 'Magnolia Klein', 'first', 54, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(3, 'Mr. Brody Watsica II', 'third', 26, '2021-06-22 03:35:45', '2021-06-22 03:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `test_results`
--

CREATE TABLE `test_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED NOT NULL,
  `test_marks` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `test_results`
--

INSERT INTO `test_results` (`id`, `student_id`, `test_marks`, `created_at`, `updated_at`) VALUES
(1, 1, 92, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(2, 2, 34, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(3, 3, 48, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(4, 4, 12, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(5, 5, 1, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(6, 6, 79, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(7, 7, 36, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(8, 8, 9, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(9, 9, 41, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(10, 10, 9, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(11, 11, 69, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(12, 12, 36, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(13, 13, 99, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(14, 14, 93, '2021-06-22 03:35:45', '2021-06-22 03:35:45'),
(15, 15, 29, '2021-06-22 03:35:45', '2021-06-22 03:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_teacher_id_foreign` (`teacher_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_results`
--
ALTER TABLE `test_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_results_student_id_foreign` (`student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_results`
--
ALTER TABLE `test_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`);

--
-- Constraints for table `test_results`
--
ALTER TABLE `test_results`
  ADD CONSTRAINT `test_results_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
