<?php

namespace Database\Factories;

use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'class_level' => $this->faker->randomElement(['freshman', 'sophomores', 'juniors', 'seniors']),
            'race' => $this->faker->randomElement(['chinese', 'malays', 'indians']),
        ];
    }
}
