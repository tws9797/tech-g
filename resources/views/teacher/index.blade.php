@extends('layouts.master')

@section('content')

    @foreach($teachers as $teacher)
        <h5>{{ $teacher->name }}'s Class ({{ $teacher->age }})</h5>
        <h5>Position: {{ ucfirst($teacher->position) }}</h5>

        <table class="table table-bordered" style="width:100%">
            <caption>List of students</caption>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Class Level</th>
                    <th>Race</th>
                    <th>Test Marks</th>
                </tr>
            </thead>
            <tbody>
                @foreach($teacher->students as $student)
                    <tr>
                        <td>{{ $student->id }}</td>
                        <td>{{ $student->name }}</td>
                        <td>{{ ucfirst($student->class_level) }}</td>
                        <td>{{ ucfirst($student->race) }}</td>
                        <td>{{ $student->testresult->test_marks }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
@endsection
