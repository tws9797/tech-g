<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Contracts\View\View;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $teachers = Teacher::with('students', 'students.testresult')->get();

        return view('teacher.index', ['teachers' => $teachers]);
    }
}
