<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';

    /**
     * One student has one test result
     *
     * @return HasOne
     */
    public function testResult(): HasOne
    {
        return $this->hasOne(TestResult::class);
    }
}
